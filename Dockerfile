FROM php:7.3-fpm-alpine as testing

WORKDIR /var/www/html
COPY ./hello.php ./
CMD ["php", "hello.php"]

FROM testing as production

RUN echo 'echo "Hello from production".PHP_EOL;' >> ./hello.php
